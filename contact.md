---
layout: default
is_contact: true
---

* Email: [will@cs.ubc.ca](mailto:will@cs.ubc.ca)

* Office: ICCS/CS X841

* Phone: [(604)822-0827](tel:6048220827)

---
