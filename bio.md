---
title: Bio
layout: default
permalink: /bio/
---

Dr. Evans is a professor of [computer science](http://www.cs.ubc.ca/) at the [University of British Columbia](http://www.ubc.ca/).