---
layout: default
---
## About Me

<img class="profile-picture" src="headshot.jpg">

I am a professor of [computer science](http://www.cs.ubc.ca/) at the [University of British Columbia](http://www.ubc.ca/).

- Ph.D., University of California, Berkeley, 1994.

- B.Sc., Yale University, 1987.


## Research Interests

I am interested in the design and analysis of algorithms, particularly ones that involve geometry, graphs, uncertainty, or compression.
Some of my current work is in on representing graphs using geometric objects with edges implied by contact or visibility, and on strategies to query moving data to establish the smallest bound on their potential congestion.
